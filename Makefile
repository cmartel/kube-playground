TAG := $(shell git describe --abbrev=0 --tags)
CI_PROJECT_DIR := $(shell pwd)

init:
	docker-compose up -d --build
	docker-compose logs -f

build:
	docker run --rm --interactive --tty --volume $(PWD)/code:/app composer install

dist:
	docker run --rm --interactive --volume $(CI_PROJECT_DIR)/code:/app composer install
	docker build -t registry.gitlab.com/cmartel/kube-playground/app -f docker/Dockerfile-app .
	docker push registry.gitlab.com/cmartel/kube-playground/app

test:
	docker run --rm --interactive --volume $(CI_PROJECT_DIR)/code:/app composer install
	docker build -t registry.gitlab.com/cmartel/kube-playground/app:$(TAG) -f docker/Dockerfile-app .
	#docker push registry.gitlab.com/cmartel/kube-playground/app:$(TAG)
	docker run -p 80:80 --rm --interactive --tty registry.gitlab.com/cmartel/kube-playground/app:$(TAG)

clean:
	docker-compose down --remove-orphans

push:
	git ci -a -m"wip"
	git push
	git tag $(NEW_TAG)
	git push --tags

kube:
	kubectl create -f app-deployment.yml
	kubectl get deployments
	#kubectl expose deployment app --type=NodePort
	kubectl expose deployment app --type=LoadBalancer
	kubectl get services
	kubectl scale deployments/app --replicas=2
	#minikube service app --url

kube-update:
	kubectl set image deployment/app app=registry.gitlab.com/cmartel/kube-playground/app:$(TAG)

kube-clean:
	kubectl delete deployment app || true
	kubectl delete service app || true

kube-health:
	watch -d -n 1  'curl -sL -w "%{http_code}\\n" "http://35.195.193.12/index.html" -o /dev/null'
