for real, for real, for realst

```shell
composer create-project drupal-composer/drupal-project:8.x-dev code --stability dev --no-interaction
docker-compose up -d --build
```

manual site install / use database connection from db env in docker-compose.yml
database export...

```
gcloud beta container --project "cellular-line-164420" clusters create "demo-cluster-1" --zone "europe-west3-a" --username="admin" --cluster-version "1.6.7" --machine-type "n1-standard-1" --image-type "COS" --disk-size "100" --scopes "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring.write","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --num-nodes "3" --network "default" --enable-cloud-logging --no-enable-cloud-monitoring --enable-legacy-authorization

gcloud beta container --project "cellular-line-164420" clusters create "demo-cluster-1" --zone "europe-west3-a" --username="admin" --scopes "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring.write","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --enable-legacy-authorization

gcloud container clusters get-credentials demo-cluster-1 --zone europe-west3-a --project cellular-line-164420
```
Cloud SQL Instanz erstellen
Ggf. User die SQL Admin Rolle vergeben
Datenbank erstellen
SQL-Dump mit USE-Zeile ergänzen
SQL-Dump in Storage Bucket und von da importieren

Dienstkonto erstellen / Schlüssel runterladen = [PROXY_KEY_FILE_PATH]
User für die DB erstellen mit [PROXY_USER] und [PROXY_PASSWORD]:
```
gcloud beta sql users create kube_playground host --instance=demo-db --password=L2mUX4tFJ4sMPdWB
```

check instance name

```
gcloud beta sql instances describe demo-db | grep connectionName
```

Secrets anlegen für den Zugriff vom Proxy auf Instanz und DB:

```
kubectl create secret generic cloudsql-instance-credentials --from-file=credentials.json=[PROXY_KEY_FILE_PATH]
kubectl create secret generic cloudsql-db-credentials --from-literal=username=[PROXY_USER] --from-literal=password=[PROXY_PASSWORD]
```

app-deployment.yml bearbeiten, instanz-connectionname anpassen

```
kubectl create -f app-deployment.yml
```

```
kubectl expose deployment app --type=LoadBalancer
```

Setting cluster acces from Gitlab: adding secrets
from kubernetes dashboard > secrets > token default

```
kubectl cluster-info
```


adapt sites/default/settings.yml
```
  'database' => 'kube_playground',
  'username' => getenv('DB_HOST'),
  'password' => getenv('DB_PASSWORD'),
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',

```